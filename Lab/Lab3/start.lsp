
;==========================================================
; Denna funktion laeser fraan stream. Extraherar ett lexeme.
; skippa white-space tecken
;==========================================================
;
(defun stream-collect-lexeme (stream ws-characters)

  (when (not (eq 'EOF (loop
		       (let ((c (peek-char nil stream nil 'EOF)))
			 (if (or (eq c 'EOF) (not (member c ws-characters)))
			     (return c)
			   (read-char stream))))))
    (let ((lexeme ""))
     (loop 
     (let ((c (peek-char nil stream nil 'EOF)))
	 (if (or (eq c 'EOF) (member c ws-characters))
	     (return lexeme)
	   (progn
	     (read-char stream)
			 ; Gah! Var fan finns string-append-char!??!?!!?
			 ; Taenk: (setf lexeme (string-append-char lexeme c))
	     (setf lexeme 
		 (concatenate 'string lexeme 
		      (make-string 1 :initial-element c))))))))))

;==========================================================
; Returvaerde fraan get-next-token aer alltid en lista.
; car  returvaerde = Token   (ex: 'IF ':= '< osv.)
; cadr returvaerde = Lexeme  (ex: "IF"  ":=" "<")
;==========================================================

(defun get-next-token (stream lexeme-mapper)
  (let ((result (stream-collect-lexeme stream '(#\Space #\Tab #\Newline))))
    (if (null result) (list 'EOF "")(funcall lexeme-mapper result))
  )
 )

;==========================================================
;Haer skapas en lista innehaallande tvaa element:
;(symbol, lexeme)
;==========================================================

(defun map-lexeme (lexeme)
(format t "The incoming lexeme is: ~S ~%" lexeme)
   (list (cond
   
; **********************************************
; ****** to be completed                  ******
; **********************************************

    ((string= lexeme "program")	'PROGRAM)
	((string= lexeme "input")	'INPUT)
	((string= lexeme "output")	'OUTPUT)
	((string= lexeme "var")		'VAR)
	((string= lexeme "integer")	'INTEGER)
	((string= lexeme "begin")	'BEGIN)
	((string= lexeme "end")		'END)
	((string= lexeme "boolean")	'BOOLEAN)
;	((string= lexeme "real")	'REAL)
	((string= lexeme "predef")	'PREDEF)
	((string= lexeme "tempty")	'TEMPTY)
	((string= lexeme "undef")	'UNDEF)
; ...
    ((string= lexeme "$")       'DOLLAR)
	((string= lexeme "(")       'LP)
	((string= lexeme ")")       'RP)
	((string= lexeme "*")       'MULTI)
	((string= lexeme "+")       'ADDI)
	((string= lexeme ",")       'COMMA)
	((string= lexeme "-")       'SUBU)
	((string= lexeme ".")       'FSTOP)
	((string= lexeme "/")       'SLASH)
	((string= lexeme ":")       'COLON)
	((string= lexeme ";")       'SCOLON)
;	((string= lexeme "=")       'EQUAL)
	((string= lexeme "")		'EOF)
	((string= lexeme ":=")		'ASSIGN)
; ...
    ((is-id lexeme)             'ID)
    ((is-number lexeme)         'NUM)
;
; **********************************************

      (t                         'UNKNOWN)
      )
    lexeme)
)

;==========================================================
;Jaemfoer om foersta tecknet i straengen aer en bokstav
;samt att resterande tillhoer maengden {a-z, A-Z, 0-9}
;==========================================================
;==========================================================

(defun is-id (str)
   (and(alpha-char-p(char str 0))
	   (every #'alphanumericp str)
   )
)

;==========================================================
;Jaemfoer om foersta tecknet i straengen aer en siffra
;samt att resterande tillhoer maengden {0-9}
;==========================================================

(defun is-number (str)
   (every #'digit-char-p str)   
)

;==========================================================
;Skapar en strukt med tvaa faelt:
;lookahead aer en lista (symbol, lexeme), stream aer en
;filstroem.
;==========================================================

(defstruct pstate
    (lookahead)
    (stream)
)

;==========================================================
;Konstruktor foer strukten pstate, initialiserar
;stream till inskickad filstroem och lookahead
;till foersta inlaesta token (symbol, lexeme)
;==========================================================

(defun create-parser-state (stream)

    (make-pstate :stream stream
                 :lookahead (get-next-token stream #'map-lexeme)
    )

)

;==========================================================
;match haemtar ett nytt token fraan filstroemmen om
;inskickad symbol matchar lookahead, annars har det 
;uppstaatt ett syntax error.
;==========================================================

(defun match (state symbol)
    
    (if (eq symbol (first (pstate-lookahead state)))
        (setf (pstate-lookahead state)
            (get-next-token (pstate-stream state) #'map-lexeme)
		)
        (format t "*** Syntax error. Expected ~A found ~A. ~%" symbol (first(pstate-lookahead state)))
    )

)

;==========================================================
;Startat programmet genom att man vid promten skriver:
;
;(parser)
;
;Programmet kommer daa att fraaga efter ett filnamn, finns
;filen kommer den att bli parserad och ett meddelande 
;kommer att skrivas ut som meddelar om parsningen gick bra.
;==========================================================

(defun parser ()
    (format t "Enter filename: ")
    (with-open-file (stream (read t) :direction :input)
       (if  (program (create-parser-state stream))
                     (format t "Parser says true. ~%")
                     (format t "Parser says false. ~%")
       )
    )
)

;==========================================================
;Opens file and reads stream
;==========================================================

(defun parsing (file)
    (with-open-file (stream file :direction :input)
       (if  (program (create-parser-state stream))
                     (format t "Parser says true. ~%")
                     (format t "Parser says false. ~%")
       )
    )
)

;==========================================================
;<program> --> <program-header><var-part><stat-part>EOF
;==========================================================

(defun program (state)
   
   (setf symtable '())			;Initializes the symbol table
   (and
		(program-header state)
		(var-part state)
		(stat-part state)
		(match state 'EOF)
	)
	
)
;*************** PROGRAM HEADER *************
;
; **********************************************
(defun program-header (state)
	(and
		(match state 'PROGRAM)
		(match state 'ID)
		(match state 'LP)
		(match state 'INPUT)
		(match state 'COMMA)
		(match state 'OUTPUT)
		(match state 'RP)
		(match state 'SCOLON)
	)
)

;*************** VAR PART *************
;
; **********************************************

(defun var-part (state)
	(and
		(match state 'VAR)
		(var-dec-list state)
	)
)

(defun var-dec-list (state)
	(and
		(var-dec state)
		(var-dec-list-tail state)
	)
)

(defun var-dec-list-tail (state)
	(if (eq (first (pstate-lookahead state)) 'ID)
		(var-dec-list state)
		t
	)
)

(defun var-dec (state)
	(and
		(id-list state)
		(match state 'COLON)
		(valType state)
		(match state 'SCOLON)
	)
)

(defun id-list (state)
	(add-to-symtable (second(pstate-lookahead state)))
	(and(match state 'ID)(id-list-tail state))
	
)

(defun id-list-tail (state)
	(if (eq (first (pstate-lookahead state)) 'COMMA)
		(progn(match state 'COMMA)(id-list state))
		t
	)
)

(defun valType (state)
	(cond
		((eq (first(pstate-lookahead state)) 'INTEGER)	(match state 'INTEGER))
		((eq (first(pstate-lookahead state)) 'BOOLEAN)	(match state 'BOOLEAN))
;		((eq (first(pstate-lookahead state)) 'REAL)		(match state 'REAL))
		(t												(match state 'INTEGER))
	)
)

;*************** SYMTABLE FUNCTIONS *************
;
;***************************************

(defun add-to-symtable (id)
	(if (eq(symtable-member id symtable) nil)	(push id symtable)
												(format t "*** Semantic Error: ~A already declared ~%" id)							
	)
)

(defun symtable-contains (id)
	(if (eq(symtable-member id symtable) nil)	(format t "*** Semantic Error: ~A not declared ~%" id)							
												)
)

(defun symtable-member (id table)
	(cond
		((null table)						nil)
		((string= (first table) id)			t)
		(t									(symtable-member id (rest table)))
	)
)
;*************** STAT PART *************
;
;***************************************

(defun stat-part (state)
	(and
		(match state 'BEGIN)
		(stat-list state)
		(match state 'END)
		(match state 'FSTOP)
	)
)

(defun stat-list (state)
	(and
		(stat state)
		(stat-list-tail state)
	)
)

(defun stat-list-tail (state)
	(if
		(string= ";" (second(pstate-lookahead state)))		(and(match state 'SCOLON)(stat-list state))
		t
	)
)

(defun stat (state)
	(assign-stat state)
)


(defun assign-stat (state)
	(and
		(match state 'ID)
		(match state 'ASSIGN)
		(expr state)
	)
)

(defun expr (state)
	(and
		(term state)
		(expr-tail state)
	)
)

(defun expr-tail (state)
	(cond
		((eq (first(pstate-lookahead state)) 'ADDI)		(and(match state 'ADDI)(expr state)))
		((eq (first(pstate-lookahead state)) 'SUBU)		(and(match state 'SUBU)(expr state)))
		(t												t									)
	)
)

(defun term (state)
	(and
		(factor state)
		(term-tail state)
	)
)

(defun term-tail (state)
	(if
		(eq (first(pstate-lookahead state)) 'MULTI)		(and(match state 'MULTI)(term state))
		t
	)
)

(defun factor (state)
	(if
		(string= "(" (second(pstate-lookahead state)))		(and(match state 'LP)(expr term)(match state 'RP))
		 													(operand state)
	)
)

(defun operand (state)
	(cond
		((is-id (second(pstate-lookahead state)))				(progn(symtable-contains (second(pstate-lookahead state)))(match state 'ID)))
		((is-number	(second(pstate-lookahead state)))			(match state 'NUM))
		(t														nil)
	)
)

; **********************************************
; ***   for testing purpose
; **********************************************

(setq testprogs '("testa.pas" "testb.pas" "testc.pas" "testd.pas" "teste.pas" "testf.pas"
	 "testg.pas" "testh.pas" "testi.pas" "testj.pas" "testk.pas" "testl.pas"
	 "testm.pas" "testn.pas" "testo.pas" "testp.pas" "testq.pas" "testr.pas"
	 "tests.pas" "testt.pas" "testu.pas" "testv.pas" "testw.pas" "testx.pas"
	 "testy.pas" "testz.pas" "testok1.pas" "testok2.pas" "testok3.pas"
	 "testok4.pas" "testok5.pas" "testok6.pas" "testok7.pas" "fun1.pas" "fun2.pas"
	 "fun3.pas" "fun4.pas" "fun5.pas" "sem1.pas" "sem2.pas" "sem3.pas" "sem4.pas"
	 "sem5.pas"))
 
 (defun testall (testprog)
	
	(cond
		((null testprog)					nil)
		(t									(format t "--------- Parsing program: ~A ~%~%" (first testprog))
											(parsing (first testprog))(format t "~%--------------------------- ~%~%" (first testprog))
											(testall (rest testprog)))
	)
 )
 
 (testall testprogs)