/** ****************** File reader ***********************************************/

read_in(File,[W|Ws])    :- see(File), get0(C), readword(C,W,C1), restsent(W,C1,Ws),seen.

restsent(W,_,[])        :- W = -1, !.
restsent(W,_,[])        :- lastword(W), !.
restsent(_,C,[W1|Ws])   :- readword(C,W1,C1), restsent(W1,C1,Ws).

readword(C,W,_)         :- C= -1, W=C.
readword(C,W,C2)		:- C= 58, get0(C1), readwordaux(C,W,C1,C2).
readword(C, W, C1) 		:- single_character( C ), !, name(W, [C]), get0(C1).

readword(C, W, C2) 		:-
   in_word_num(C, NewC ),
   !,
   get0(C1),
   restnum(C1, Cs, C2),
   name(W, [NewC|Cs]).

readword(C, W, C2) 		:-
   in_word(C, NewC ),
   !,
   get0(C1),
   restword(C1, Cs, C2),
   name(W, [NewC|Cs]).
   
 readword(_,W,C2)		:- get0(C1), readword(C1,W,C2).
 
 readwordaux(C,W,C1,C2)			:- C1 = 61, name(W,[C,C1]), get0(C2).
 readwordaux(C,W,C1,C2)			:- C1 \= 61, name(W,[C]), C1=C2.

 restnum(C, [NewC|Cs], C2)	:- in_word_num(C,NewC), !, get0(C1), restnum(C1,Cs,C2). 
 restnum(C,[],C).
 
 
 restword(C, [NewC|Cs], C2)	:- in_word(C,NewC), !, get0(C1), restword(C1,Cs,C2).
 restword(C,[],C).
 
single_character(40).                  /* ( */
single_character(41).                  /* ) */
single_character(42).                  /* + */
single_character(43).                  /* * */
single_character(44).                  /* , */
single_character(59).                  /* ; */
single_character(58).                  /* : */
single_character(61).                  /* = */
single_character(46).                  /* . */

in_word(C, C) :- C>96, C<123.             /** a b ... z */
in_word(C, L) :- C>64, C<91, L is C+32.   /** A B ... Z */
in_word(C, C) :- C>47, C<58.              /** 1 2 ... 9 */
in_word_num(C, C) :- C>47, C<58.              /** 1 2 ... 9 */

lastword('.').

/** ****************** Lexer ***********************************************/

lexem([],[]).
lexem([H|T], [F|S]) :- match(H,F), lexem(T,S).

/** ****************** Match ***********************************************/

match(L,T) 				:- 	 	L='$', 			T is 36, !.
match(L,T) 				:- 		L='(', 			T is 40, !.
match(L,T) 				:- 		L=')', 			T is 41, !.
match(L,T) 				:- 		L='*', 			T is 42, !.
match(L,T) 				:- 		L='+', 			T is 43, !.
match(L,T) 				:- 		L=',', 			T is 44, !.
match(L,T) 				:- 		L='-', 			T is 45, !.
match(L,T) 				:- 		L='.', 			T is 46, !.
match(L,T) 				:- 		L='/', 			T is 47, !.
match(L,T) 				:- 		L=':', 			T is 58, !.
match(L,T) 				:- 		L=';', 			T is 59, !.
match(L,T) 				:- 		L='=', 			T is 61, !.

match(L,T) 				:- 		L='program', 	T is 256, !.
match(L,T) 				:- 		L='input', 		T is 257, !.
match(L,T) 				:- 		L='output', 	T is 258, !.
match(L,T) 				:-  	L='var', 		T is 259, !.
match(L,T) 				:-  	L='integer', 	T is 260, !.
match(L,T) 				:- 		L='begin', 		T is 261, !.
match(L,T) 				:- 		L='end', 		T is 262, !.
match(L,T) 				:-  	L='boolean', 	T is 263, !.
match(L,T) 				:-  	L='real', 		T is 264, !.
match(L,T) 				:- 		L='predef', 	T is 267, !.
match(L,T) 				:- 		L='tempty',	 	T is 268, !.
match(L,T) 				:- 		L='undef', 		T is 269, !.
match(L,T)				:- 		name(L,[H|Tail]), char_type(H,alpha),!,match_alnum(Tail),T is 270.
match_alnum([]).
match_alnum([H|T])		:-		char_type(H,alnum),match_alnum(T).

match(L,T) 				:-  	L=':=', 		T is 271, !.

match(L,T)				:-		name(L,[H|Tail]), char_type(H,digit),!,match_num(Tail),T is 272.
match_num([]).
match_num([H|T])		:-		char_type(H,digit),!,match_num(T).

match(L,T) 				:- 		L = -1, 		T is 273.			/** Error */

/** ****************** Pars Matcher ***********************************************/

lpar	-->		[40].
rpar	-->		[41].
mult	-->		[42].
addi	-->		[43].
comm	-->		[44].
fs		-->		[46].
col		-->		[58].
semcol	-->		[59].

program	-->		[256].
input	-->		[257].
output	-->		[258].
var		-->		[259].
integer -->		[260].
begin	-->		[261].
end		-->		[262].
boolean	-->		[263].
real	-->		[264].
id		-->		[270].
assign	-->		[271].
number	-->		[272].



/** ****************** Parser ***********************************************/

parser(L,Res)					:- write(L), nl, (prog(L,Res), Res=[], write('Parse OK!'));
									write('Parse Fail!').

prog				--> prog_header, var_part, stat_part.
prog_header			--> program, id, lpar, input, comm, output, rpar, semcol.

var_part			--> var, var_dec_list.

var_dec_list		--> var_dec.
var_dec				--> id_list, col, type, semcol, var_dec.
var_dec				--> [].
id_list				--> id, comm, id_list.
id_list				--> id.
type				--> integer; real; boolean.

stat_part			--> begin, stat_list, end, fs.
stat_list			-->	stat, semcol, stat_list.
stat_list			--> stat.
stat				--> assign_stat.
assign_stat			--> id, assign, expr.
expr				--> term, addi, expr.
expr				--> term.
term				--> factor, mult, term.
term				--> factor. 
factor				--> (lpar, expr, rpar); operand.
operand				--> id; number.

/** ****************** Tests ***********************************************/

files(['testok1.pas','testok2.pas','testok3.pas','testok4.pas','testok5.pas','testok6.pas','testok7.pas',
		'testa.pas','testb.pas','testc.pas','testd.pas','teste.pas','testf.pas','testg.pas','testh.pas',
		'testi.pas','testj.pas','testk.pas','testl.pas','testm.pas','testn.pas','testo.pas','testp.pas',
		'testq.pas','testr.pas','tests.pas','testt.pas','testu.pas','testv.pas','testw.pas','testx.pas',
		'testy.pas','testz.pas','fun1.pas','fun2.pas','fun3.pas','fun4.pas','fun5.pas','sem1.pas','sem2.pas',
		'sem3.pas','sem4.pas','sem5.pas']).

parseFiles([]).
parseFiles([H|T])		:- write('Testing '), write(H), nl,
							read_in(H,L), write(L), nl, lexem(L,Tokens), parser(Tokens,_),nl,
							write(H), write(' end'), nl,nl,parseFiles(T).

testall					:- tell('library.out'), files(L), parseFiles(L), told.