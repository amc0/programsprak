/**********************************************************************/
/* lab 1 DVG C01 - Parser OBJECT                                      */
/**********************************************************************/

/**********************************************************************/
/* Include files                                                      */
/**********************************************************************/
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>

/**********************************************************************/
/* Other OBJECT's METHODS (IMPORTED)                                  */
/**********************************************************************/
#include "keytoktab.h"      /* when the keytoktab is added   */
#include "lexer.h"          /* when the lexer     is added   */
#include "symtab.h"         /* when the symtab    is added   */
#include "optab.h"          /* when the optab     is added   */

/**********************************************************************/
/* OBJECT ATTRIBUTES FOR THIS OBJECT (C MODULE)                       */
/**********************************************************************/
static int	DEBUG=1;
static int  lookahead=0;
static int  is_parse_ok=0;
static toktyp  rVal=undef;
static int LVal = -1;

/**********************************************************************/
/* RAPID PROTOTYPING - simulate the token stream & lexer (get_token)  */
/**********************************************************************/
/* define tokens + keywords NB: remove this when keytoktab.h is added */
/**********************************************************************/
//enum tvalues { program = 257, id, input, output };
/**********************************************************************/
/* Simulate the token stream for a given program                      */
/**********************************************************************/
/*static int tokens[] = {program, id, '(', input, ',', output, ')', ';',
               '$' };*/
/*static int tokens[] = {program, id, '(', input, ',', output, ')', ';',
				var, id, ',', id, ',', id, ':', integer, ';',
				begin,
					id, assign, id, '+', '(', id, '*', number, ')',
					end, '.',
				 '$' };*/

/**********************************************************************/
/*  Simulate the lexer -- get the next token from the buffer          */
/**********************************************************************/
/*static int pget_token()
{  
   static int i=0;
   if (tokens[i] != '$') return tokens[i++]; else return '$';
   }*/
static int pget_token()
{
	return get_token();
}
/**********************************************************************/
/*  PRIVATE METHODS for this OBJECT  (using "static" in C)            */
/**********************************************************************/
int expr();

static void skipTok(){ if(DEBUG) printf("Skipping token: %s\n", get_lexeme());
						lookahead = pget_token(); }
						
static void printRest(){
	printf("\nSYNTAX: Extra symbols after end of parse!\n	");
	while(!((strcmp(get_lexeme(),"\0")==0)||(strcmp(get_lexeme(),"$")==0))){
		printf("%s ", get_lexeme());
		lookahead = pget_token();
	}
}

static void setRVal(toktyp val){ rVal = val; }
static toktyp getRVal(){ return rVal; }

/**********************************************************************/
/* The Parser functions                                               */
/**********************************************************************/
static void match(int t)
{
	if(DEBUG) printf("\n *** In match");
	if(lookahead == t){ 
	   if(DEBUG) printf("	expected: %7s | found %7s | (Lexeme: %s)",
							tok2lex(t), tok2lex(lookahead), get_lexeme());
       lookahead = pget_token(); }
   else {
      is_parse_ok=0;
      if(DEBUG) printf("\nSYNTAX:	 Symbol expected: %7s | found: %7s | (Lexeme: %s)",
              tok2lex(t), tok2lex(lookahead), get_lexeme());
      }
   }

/**********************************************************************/
/* The grammar functions                                              */
/**********************************************************************/
/**********************************************************************/
/*  Stat part								                          */
/**********************************************************************/
void operand()
{
	if(DEBUG) printf("\n *** In operand\n");
	switch(lookahead){
		case id: 
			if(!find_name(get_lexeme())){
				printf("SEMANTIC: ID NOT declared: %s\n",get_lexeme());
				skipTok();
				break;
			}
			else match(id); break;
		case number: match(number); break;
		default: printf("Operand not supported");
	}
	
}
int factor()
{
	int value;
	
	if(DEBUG) printf("\n *** In factor");
	if(lookahead=='('){ match('('); expr(); match(')');}
	else if(lookahead==id){ 	LVal=get_ntype(get_lexeme()); operand(); }
	else if(lookahead==number){ value=integer; operand(); }
	else{	printf("\nSYNTAX: SKIPPING UNEXPECTED SYMBOL (in factor) %s\n",
			get_lexeme()); skipTok();
			LVal=error;
		}
	if(DEBUG) printf("Value: %s\n", tok2lex(value));
	printf("\nErrval: %s", tok2lex(value));
	return LVal;
}

int R2()
{
	if(DEBUG) printf("\n *** In R2");
	//if(lookahead=='*'){ match('*'); return get_otype('*', factor(), R2()); } //get_otype
	if(lookahead=='*'){ match('*'); 
		LVal = get_otype('*', R2(), factor());
						printf("\nR2val: %s", tok2lex(LVal));
		/*printf("\nR2val: %s", tok2lex(get_otype('*', R2(), factor())));
		return get_otype('*', R2(), factor()); } //get_otype*/
		return LVal;}
	else return LVal;
}

int term()
{
	if(DEBUG) printf("\n *** In term");
	//return get_otype('*',factor(), R2()); //get_otype
	int termtok;
	int factorVal = factor();
	int R2Val = R2();
	if(R2Val!=-1){ LVal=termtok = get_otype('*',R2Val, factorVal); }
	else{ termtok=LVal=factorVal; }
	printf("\nTermval: %s", tok2lex(termtok));
	return termtok;
	/*printf("\nTermval: %s", tok2lex(get_otype('*',R2(), factor())));
	return get_otype('*',R2(), factor()); //get_otype*/
}

int R1()
{
	if(DEBUG) printf("\n *** In R1");
	//if(lookahead=='+'){ match('+'); return get_otype('+', term(), R1()); } //get_otype
	if(lookahead=='+'){ match('+'); 
		LVal = get_otype('+', R1(), term());
						printf("\nR1val: %s", tok2lex(LVal));
		return LVal;}
						/*printf("\nR1val: %s", tok2lex(get_otype('+', R1(), term())));
						return get_otype('+', R1(), term()); } //get_otype*/
	else return LVal;
}

int expr()
{
	if(DEBUG) printf("\n *** In expr");
	//return get_otype('+',term(),R1()); //get_otype
	int exprtok;
	int termVal = term();
	int R1Val =R1();
	if(R1Val!=-1){ exprtok = get_otype('+',R1(),term()); }
	else{ exprtok = LVal = termVal; }
	//printf("\nexprval: %s", tok2lex(get_otype('+',R1(),term())));
	printf("\nexprval: %s", tok2lex(exprtok));
	return exprtok;
	/*printf("\nexprval: %s", tok2lex(get_otype('+',R1(),term())));
	return get_otype('+',R1(),term());*/
}
void assign_stat()
{
	if(DEBUG){ 	printf("\n *** In assign_stat");
				printf("\nLexeme: %s\n",get_lexeme()); 
	}
	if(lookahead==id&&(!find_name(get_lexeme()))){
		printf("SEMANTIC: Identifier %s not declared.\n",get_lexeme());
		skipTok();
	}
	else{ printf("Lexeme: %s\n", get_lexeme()); setRVal(get_ntype(get_lexeme())); match(id); printf("RVal: %s\n", tok2lex(getRVal())); }
	match(assign);
	int end = expr();
	if(getRVal()==end) printf("Success!");
	else
		printf("\nNot working! RVal: %s End: %s\n",tok2lex(getRVal()),tok2lex(end));
}

void stat()
{
	if(DEBUG) printf("\n *** In stat");
	assign_stat();
}

void stat_list()
{
	if(DEBUG) printf("\n *** In stat_list");
	stat();if(lookahead==';'){ match(';'); stat_list();  }
}

void stat_part()
{
	if(DEBUG) printf("\n *** In stat_part");
	match(begin);
	stat_list();
	match(end);
	match('.');
	if(!(strcmp(get_lexeme(),"$")==0)){
		printRest();
	}
}

/**********************************************************************/
/*  Var part								                          */
/**********************************************************************/

void type()
{
	if(DEBUG) printf("\n *** In type");
	switch(lookahead){
		case integer: match(integer); setv_type(integer); break;
		case boolean: match(boolean); setv_type(boolean); break;
		case real:	  match(real); setv_type(real);	  break;
		default: printf("\nSYNTAX:   Type name expected found %s",get_lexeme()); setv_type(error); break;
	}
}

void id_list(){
	if(DEBUG) printf("\n *** In id_list\n");
	printf("\nLexeme: %s\n",get_lexeme());
	if(find_name(get_lexeme())){
		printf("SEMANTIC: Identifier: %s already declared.\n", get_lexeme());
		//skipTok();
	}
	else if(lookahead==id){
		addv_name(get_lexeme());
	}
	match(id);
	if(lookahead==','){
		match(',');
		id_list();
	}
}

void var_dec()
{
	if(DEBUG) printf("\n *** In var_dec");
	id_list();match(':');type();match(';');
	if(lookahead==id){ var_dec();}
}

void var_dec_list()
{
	if(DEBUG) printf("\n *** In var_dec_list");
	var_dec(); 
	//if(lookahead==id){ var_dec_list();}
}

void var_part()
{
	if(DEBUG) printf("\n *** In var_part");
	match(var);
	var_dec_list();
}

/**********************************************************************/
/*  Program Header							                          */
/**********************************************************************/

static void program_header()
{
   if (DEBUG) printf("\n *** In  program_header");
   match(program); 
   if(lookahead==id) addp_name(get_lexeme());
   else addp_name("???");
   match(id); match('('); 
   match(input);   match(','); match(output); match(')'); match(';');
   }
   
/**********************************************************************/
/*  PUBLIC METHODS for this OBJECT  (EXPORTED)                        */
/**********************************************************************/

int parser()
{
	if (DEBUG) printf("\n *** In  parser\n");
	if(!isatty(STDIN_FILENO)){
		FILE *stdin_stream = stdin;
		fseek(stdin_stream,-4,SEEK_END);
		if(ftell(stdin_stream)>0){
		   rewind(stdin_stream);
		   lookahead = pget_token();       // get the first token
		   program_header();               // call the first grammar rule
		   var_part();
		   stat_part();
		   is_parse_ok=1;
		   p_symtab();  
	   }
	   else
		printf("SYNTAX:   Input file is empty \n");
	}
	else
	printf("SYNTAX:   Input file is expected \n");
	return is_parse_ok;             // status indicator
}

/**********************************************************************/
/* End of code                                                        */
/**********************************************************************/

