/**********************************************************************/
/* lab 1 DVG C01 - Symbol Table OBJECT                                */
/**********************************************************************/

/**********************************************************************/
/* Include files                                                      */
/**********************************************************************/
#include <stdio.h>
#include <ctype.h>
#include <string.h>

/**********************************************************************/
/* Other OBJECT's METHODS (IMPORTED)                                  */
/**********************************************************************/
#include "keytoktab.h"

/**********************************************************************/
/* OBJECT ATTRIBUTES FOR THIS OBJECT (C MODULE)                       */
/**********************************************************************/
#define TABSIZE 1024                   /* symbol table size           */
#define NAMELEN   20                   /* name length                 */

typedef char tname[NAMELEN];

static tname  name[TABSIZE];
static toktyp role[TABSIZE];
static toktyp type[TABSIZE];
static int    size[TABSIZE];
static int    addr[TABSIZE];

static int numrows=0;                  /* number of rows in the ST    */
static int startp =0;                  /* start position program in ST*/
static int nextAddr=0;
static int DEBUG = 0;

/**********************************************************************/
/*  PRIVATE METHODS for this OBJECT  (using "static" in C)            */
/**********************************************************************/
int find_name(char * fpname); 
/**********************************************************************/
/*  GET methods (one for each attribute)                              */
/**********************************************************************/
static char * get_name(int ftref)   { return name[ftref]; }
static toktyp get_role(int ftref)   { return role[ftref]; }
static toktyp get_type(int ftref)   { return type[ftref]; }
static int    get_size(int ftref)   { return size[ftref]; }
static int    get_addr(int ftref)   { return addr[ftref]; }

/**********************************************************************/
/*  SET methods (one for each attribute)                              */
/**********************************************************************/
static void set_name(int ftref, char * fpname) { strcpy(name[ftref],fpname);}
static void set_role(int ftref, toktyp frole)  { role[ftref] = frole; }
static void set_type(int ftref, toktyp ftype)  { type[ftref] = ftype; }
static void set_size(int ftref, int    fsize)  { size[ftref] = fsize; }
static void set_addr(int ftref, int    faddr)  { addr[ftref] = faddr; }

/**********************************************************************/
/*  Add a row to the symbol table                                     */
/**********************************************************************/
static void addrow(char *fname, toktyp frole, toktyp ftype, 
                   int fsize, int faddr)
{
    if(DEBUG) printf("\n ADDROW \n");
    set_name(numrows, fname);
    set_role(numrows, frole);
    set_type(numrows, ftype);
    set_size(numrows, fsize);
    set_addr(numrows, faddr);
	numrows++;
}
/**********************************************************************/
/*  Initialise the symbol table                                       */
/**********************************************************************/
static void initst()
{
    addrow(tok2lex(predef),  typ, predef, 0, 0);
    addrow(tok2lex(undef),   typ, predef, 0, 0);
    addrow(tok2lex(error),   typ, predef, 0, 0);
    addrow(tok2lex(integer), typ, predef, 4, 0);
    addrow(tok2lex(boolean), typ, predef, 4, 0);
    addrow(tok2lex(real),    typ, predef, 8, 0);
    startp=numrows;
}

/**********************************************************************/
/*  return a reference to the ST (index) if name found else nfound    */
/**********************************************************************/
 static int get_ref(char * fpname)
 {
	if(DEBUG) printf("\n *** IN get_ref()");
	int i=0;
	while(i<numrows){
		if(strcmp(get_name(i),fpname)==0)	return i;
		else i++;
	}
	return 0;

 }
/**********************************************************************/
/*  get/set next adress for variables and program                     */
/**********************************************************************/
 static int get_nextAddr(){ return nextAddr; }
 
 static void set_nextAddr(int size){ nextAddr += size; }
/**********************************************************************/
/*  PUBLIC METHODS for this OBJECT  (EXPORTED)                        */
/**********************************************************************/
/**********************************************************************/
/*  Display the symbol table                                          */
/**********************************************************************/
static void p_symrow(int ftref)
{
   printf("%12s%10s%10s%10d%10d\n", 
		   get_name(ftref),tok2lex(get_role(ftref)),tok2lex(get_type(ftref)),
		   get_size(ftref),get_addr(ftref));
}

void p_symtab()
{
	if(DEBUG) printf("Numrows: %d\n",numrows);
	if(DEBUG) printf("\n *** IN p_symtab()\n");
	printf("\n________________________________________________________\n");
	printf(" THE SYMBOL TABLE\n");
	printf("________________________________________________________\n");
	printf("       NAME       ROLE       TYPE      SIZE      ADDR\n");
	printf("________________________________________________________\n");
	
	int i=startp;
	while(i<numrows){
		p_symrow(i);
		i++;
	}
	printf("________________________________________________________\n");
	printf(" STATIC STORAGE REQUIRED is %d BYTES\n", get_nextAddr());
	printf("________________________________________________________\n");
}

/**********************************************************************/
/*  Add a program name to the symbol table                            */
/**********************************************************************/
void addp_name(char * fpname) { 

	if(numrows==0) initst();
	addrow(fpname,lex2tok("program"),lex2tok("program"),0,0);

}

/**********************************************************************/
/*  Add a variable name to the symbol table                           */
/**********************************************************************/

void addv_name(char * fpname) { addrow(fpname,lex2tok("var"),lex2tok("undef"),0,0); }

/**********************************************************************/
/*  Find a name in the the symbol table                               */
/*  return a Boolean (true, false) if the name is in the ST           */
/**********************************************************************/
int find_name(char * fpname) { 
	int i=0;
	while(i<numrows){ if(strcmp(get_name(i),fpname)==0){ return 1; }
		i++;
	}
	return 0;
}

/**********************************************************************/
/*  Set the type of an id list in the symbol table                    */
/**********************************************************************/
void setv_type(toktyp ftype) {
	int i=startp;
	while(i<numrows){
		if(get_type(i)==undef){
			set_type(i, ftype);
			set_size(i, get_size(get_ref(tok2lex(ftype))));
			set_addr(i,get_nextAddr());
			set_nextAddr(get_size(i));
			set_size(startp,get_nextAddr());
		}
		i++;
	}
}

/**********************************************************************/
/*  Get the type of a variable from the symbol table                  */
/**********************************************************************/
toktyp get_ntype(char * fpname) { 

	if(find_name(fpname))	return get_type(get_ref(fpname));
	else 					return get_type(get_ref("undef"));
}

/**********************************************************************/
/* End of code                                                        */
/**********************************************************************/
