/**********************************************************************/
/* lab 1 DVG C01 - Lexer OBJECT                                       */
/**********************************************************************/

/**********************************************************************/
/* Include files                                                      */
/**********************************************************************/
#include <stdio.h>
#include <ctype.h>
#include <string.h>

/**********************************************************************/
/* Other OBJECT's METHODS (IMPORTED)                                  */
/**********************************************************************/
#include "keytoktab.h"

/**********************************************************************/
/* OBJECT ATTRIBUTES FOR THIS OBJECT (C MODULE)                       */
/**********************************************************************/
#define BUFSIZE 1024
#define LEXSIZE   30

static int DEBUG=0;
static char buffer[BUFSIZE];
static char lexbuf[LEXSIZE];
static int  pbuf  = 0;               /* current index program buffer  */
static int  plex  = 0;               /* current index lexeme  buffer  */

/**********************************************************************/
/*  PRIVATE METHODS for this OBJECT  (using "static" in C)            */
/**********************************************************************/
/**********************************************************************/
/* buffer functions                                                   */
/**********************************************************************/
/**********************************************************************/
/* Read the input file into the buffer                                */
/**********************************************************************/

static void get_prog()
{
	if(DEBUG) printf("Get_prog()\n");
	if(stdin!=NULL){
		while((buffer[pbuf]=fgetc(stdin))!=EOF){ printf("%c",buffer[pbuf]); pbuf++;}
		buffer[pbuf]='$';
	}
	if(DEBUG) printf("Done reading\n");
	pbuf=0;
}     

/**********************************************************************/
/* Display the buffer                                                 */
/**********************************************************************/  

static void pbuffer()
{   
	printf("________________________________________________________\n");
	printf(" THE PROGRAM TEXT\n");
	printf("________________________________________________________\n");
	while(buffer[pbuf]!=EOF && pbuf<BUFSIZE){
		printf("%c", buffer[pbuf]);
		pbuf++;
	}
	printf("\n________________________________________________________\n");
	pbuf=0;
}

/**********************************************************************/
/* Copy a character from the program buffer to the lexeme buffer      */
/**********************************************************************/

static void get_char(){ lexbuf[plex++]=buffer[pbuf++]; lexbuf[plex] = '\0'; }
	
/**********************************************************************/
/* End of buffer handling functions                                   */
/**********************************************************************/

/**********************************************************************/
/*  PUBLIC METHODS for this OBJECT  (EXPORTED)                        */
/**********************************************************************/

/**********************************************************************/
/* Return a token                                                     */
/**********************************************************************/
int get_token()
{
	if(pbuf==0){ get_prog(); pbuffer();}
	memset(&lexbuf[0],0,plex); plex=0;
	
	while(isspace(buffer[pbuf])) pbuf++; 
	
	if(isalpha(buffer[pbuf])) {
		while(isalnum(buffer[pbuf])) get_char();
		return key2tok(lexbuf);
	}
	else if (isdigit(buffer[pbuf])){
		while(isdigit(buffer[pbuf])) get_char();
		return number;
	}
	else {
		get_char();
		if (lexbuf[plex-1] == ':' && buffer[pbuf] == '=') get_char();
		return lex2tok(lexbuf);
	}
}
/**********************************************************************/
/* Return a lexeme                                                    */
/**********************************************************************/
char * get_lexeme()
{ return lexbuf; }

/**********************************************************************/
/* End of code                                                        */
/**********************************************************************/


