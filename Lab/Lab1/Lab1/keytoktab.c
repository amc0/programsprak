/**********************************************************************/
/* lab 1 DVG C01 - Driver OBJECT                                      */
/**********************************************************************/

/**********************************************************************/
/* Include files                                                      */
/**********************************************************************/
#include <stdio.h>
#include <ctype.h>
#include <string.h>

/**********************************************************************/
/* Other OBJECT's METHODS (IMPORTED)                                  */
/**********************************************************************/
#include "keytoktab.h"

/**********************************************************************/
/* OBJECT ATTRIBUTES FOR THIS OBJECT (C MODULE)                       */
/**********************************************************************/
/**********************************************************************/
/* type definitions                                                   */
/**********************************************************************/
typedef struct tab {
	char 	* text;
	int 	token;
	} tab;
	
/**********************************************************************/
/* data objects (tables)                                              */
/**********************************************************************/
static tab tokentab[ ] = {
        {"id", 	        id},
	{"number", 	number},
	{":=", 	        assign},
	{"undef", 	undef},
	{"predef", 	predef},
	{"tempty", 	tempty},
	{"error",        error},
	{"type",         typ},
	{"$",            '$'},
	{"(",            '('},
	{")",            ')'},
	{"*",            '*'},
	{"+",            '+'},
	{",",            ','},
	{"-",            '-'},
	{".",            '.'},
	{"/",            '/'},
	{":",            ':'},
	{";",            ';'},
	{"=",            '='},
	{"TERROR", 	nfound}
        };


static tab keywordtab[ ] = {
	{"program", 	program},
	{"input", 	input},
	{"output", 	output},
	{"var", 	var},
	{"begin", 	begin},
	{"end", 	end},
	{"boolean", 	boolean},
	{"integer", 	integer},
	{"real", 	real},
	{"KERROR", 	nfound}
	} ;
   
/**********************************************************************/
/*  PUBLIC METHODS for this OBJECT  (EXPORTED)                        */
/**********************************************************************/
/**********************************************************************/
/* Display the tables                                                 */
/**********************************************************************/
void p_toktab()
{  
	int i=0, j=0;
	printf("----------- PROGRAM KEYWORDS -----------\n");
	while(keywordtab[i].token!=nfound){
		printf("\t%8s  %5i\n",keywordtab[i].text, keywordtab[i].token);
		i++;
	}
	printf("----------- PROGRAM TOKENS -----------\n");
	while(tokentab[j].token!=nfound){
		printf("\t%8s  %8i\n",tokentab[j].text, tokentab[j].token);
		j++;
	}
}

/**********************************************************************/
/* lex2tok - convert a lexeme to a token                              */
/**********************************************************************/
toktyp lex2tok(char * fplex)
{
   int i=0;
	while(tokentab[i].token!=nfound){
		if(strcmp(tokentab[i].text,fplex)==0) return tokentab[i].token;
		i++;
	}
	return key2tok(fplex);
}

/**********************************************************************/
/* key2tok - convert a keyword to a token                             */
/**********************************************************************/
toktyp key2tok(char * fplex)
{
   int i=0;
	while(keywordtab[i].token!=nfound){
		if(strcmp(keywordtab[i].text,fplex)==0) return keywordtab[i].token;
		i++;
	}
	return id;
   }

/**********************************************************************/
/* tok2lex - convert a token to a lexeme                              */
/**********************************************************************/
char * tok2lex(toktyp ftok)
{
	int i=0, j=0;
	while(keywordtab[i].token!=nfound){
		if(keywordtab[i].token == ftok) return keywordtab[i].text;
		i++;
	}
	while(tokentab[j].token!=nfound){
		if(tokentab[j].token == ftok) return tokentab[j].text;
		j++;
	}
	return tokentab[j].text;
   }

/**********************************************************************/
/* End of code                                                        */
/**********************************************************************/

